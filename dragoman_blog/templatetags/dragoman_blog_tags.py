from django import template
from django.contrib.auth.models import User, Permission
from django.conf import settings
from django import template
from django.contrib.auth import models as auth_models
from dragoman_blog.models import EntryTranslation

register = template.Library()

@register.inclusion_tag('admin/dragoman_blog/submit_line.html', takes_context=True)
def submit_row(context):
    """
    Displays the row of buttons for delete and save.
    """
    opts = context['opts']
    change = context['change']
    is_popup = context['is_popup']
    save_as = context['save_as']
    ctx = {
        'opts': opts,
        'onclick_attrib': (opts.get_ordered_objects() and change
                            and 'onclick="submitOrderForm();"' or ''),
        'show_delete_link': (not is_popup and context['has_delete_permission']
                              and change and context.get('show_delete', True)),
        'show_save_as_new': not is_popup and change and save_as,
        'show_save_and_add_another': context['has_add_permission'] and
                            not is_popup and (not save_as or context['add']),
        'show_save_and_continue': not is_popup and context['has_change_permission'],
        'is_popup': is_popup,
        'show_save': True
    }
    if context.get('original') is not None:
        ctx['original'] = context['original']
    if context.get('translation_language_code') is not None:
        ctx['translation_language_code'] = context['translation_language_code']
    if context.get('translation_language_field') is not None:
        ctx['translation_language_field'] = context['translation_language_field']
    return ctx

@register.inclusion_tag('dragoman_blog/tags_show_authors.html', name='blog_show_authors', takes_context=True)
def show_authors(context, order_by='username'):
    request = context["request"]
    #language = get_language_from_request(request)
    #info = translation_pool.get_info(Entry)
    #model = info.translated_model
    #kw = get_translation_filter_language(Entry, language)
    context.update({
        'authors': auth_models.User.objects.filter(
						pk__in=EntryTranslation.objects.all().values('author')
						).order_by(order_by).values_list('username', flat=True)[:5]
    })
    return context


